module.exports = {
  content: [
    './src/**/*.{js,vue,ts,html}',
    './site/templates/**/*.php',
    './site/plugins/**/*.blade.php'
  ],
  theme: {
    extend: {
      colors: {
        navigation: '#30384C',
        dark: '#2B2B2B',
      },
      fontFamily: {
        sans: ['Varela Round', 'sans-serif'],
        serif: ['Nickainley-Normal', 'serif'],
      },
    },
  },
  plugins: [],
}
