import {createApp} from 'vue'
import 'virtual:svg-icons-register'
import SetAverageColor from '@lib/average-color.js'
import BurgerGetColor from '@lib/burger-get-color.js'
import Rellax from 'rellax'

// Import all blade vue components
const components = import.meta.globEager('./blade/*.vue')

window.App = (() => {
  return {
    liftOff: (appSelector) => {
      const vueApp = createApp({
        data() {
          return {
            ...window.AppData.data,
          }
        },

        methods: {
          ...window.AppData.methods,
        },

        mounted() {
          SetAverageColor().then(BurgerGetColor)
          const rellax = new Rellax('.rellax')
        },
      })

      Object.entries(components).forEach(([path, definition]) => {
        // Get name of component, based on filename
        // './components/Fruits.vue' will become 'Fruits'
        const componentName = path.split('/').pop().replace(/\.\w+$/, '')

        // Register component on this Vue instance
        vueApp.component(componentName, definition.default)
      })

      for (const m of Object.values(import.meta.globEager('./modules/*.js'))) {
        m.install?.(vueApp)
      }

      vueApp.mount(appSelector)
    },
  }
})(createApp, components)
