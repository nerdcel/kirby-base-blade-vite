const componentToHex = (c) => {
  const hex = c.toString(16)
  return hex.length == 1 ? '0' + hex : hex
}

const rgbToHex = (r, g, b) => {
  return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b)
}

const rgbsToRgba = (str) => {
  const matches = str.match(/rgb\((\d+),\s(\d+),\s(\d+)\)/)
  const matches2 = str.match(/rgba\((\d+),\s(\d+),\s(\d+),\s(\d+)\)/)

  if (matches) {
    return [
      Number(matches[1]),
      Number(matches[2]),
      Number(matches[3])
    ]
  }
  if (!matches2 || matches2[4] === '0') {
    return [255, 255, 255];
  }
  return [
      Number(matches2[1]),
      Number(matches2[2]),
      Number(matches2[3]),
      Number(matches2[4])
    ]
}

const hexToRgb = (hex) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
  } : null
}

const isDark = (color) => {
    const result = (color[0] * 299 + color[1] * 587 + color[2] * 114) / 1000;
    return result < 128;
}

export {
  componentToHex,
  rgbToHex,
  hexToRgb,
  isDark,
  rgbsToRgba
}
