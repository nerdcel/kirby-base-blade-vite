import FastAverageColor from 'fast-average-color'
import { isDark, rgbsToRgba, rgbToHex } from '@lib/color-conversion.js'

export default async (selector = '#app', sections = 'section') => {
  const app = document.querySelector(selector)
  const colorSections = app.querySelectorAll(sections)
  const fac = new FastAverageColor()

  colorSections.forEach((node) => {
    const img = node.querySelector('img')

    fac.getColorAsync(img)
      .then(color => {
        node.setAttribute('data-avg-color', color.rgb)
        node.setAttribute('data-is-dark', color.isDark)
      })
      .catch(e => {
        let style
        if (window.getComputedStyle) {
          style = window.getComputedStyle(node)
        } else {
          style = node.currentStyle
        }

        if (!style) {
          node.setAttribute('data-avg-color', '#ffffff')
          return node.setAttribute('data-is-dark', false)
        }

        const bg = rgbsToRgba(style.backgroundColor);
        node.setAttribute('data-avg-color', rgbToHex(...bg))
        return node.setAttribute('data-is-dark', isDark(bg))
      })
  })

  return new Promise(resolve => resolve());
}
