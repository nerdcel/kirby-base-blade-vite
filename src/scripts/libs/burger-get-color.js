import requestAnimation from '@lib/request-animation.js'
import cheap from '@lib/cheap.js'

const checkColor = () => {
  const burger = document.querySelector('.nav-burger')
  const {offsetTop, offsetLeft} = burger
  const elements = document.elementsFromPoint(offsetLeft, offsetTop)
  const section = elements.filter((elem) => {
    return elem.hasAttribute('data-avg-color')
  })

  cheap(section.length && !section[0].hasAttribute('data-current'), () => {
    document.querySelectorAll('[data-current]').forEach((node) => node.removeAttribute('data-current'))
    section[0].setAttribute('data-current', 'yes');
    if (section[0].getAttribute('data-is-dark') === 'false') {
      return burger.classList.add('is-dark')
    }
    return burger.classList.remove('is-dark')
  })
}

const requestColor = requestAnimation(checkColor)

export default () => {
  checkColor()
  window.addEventListener('scroll', requestColor)
}
