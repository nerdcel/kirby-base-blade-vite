export default (condition, callback) => {
  if (condition) {
    return callback();
  }
  return condition;
}
