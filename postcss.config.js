import tailwind from 'tailwindcss';
import autoprefixer from 'autoprefixer';
import tailwindConfig from './tailwind.config'
import postCssLock from 'postcss-csslock'

export default {
  plugins: [tailwind(tailwindConfig), autoprefixer, postCssLock]
}
