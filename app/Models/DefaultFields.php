<?php

namespace Nerdcel\Models;

use Kirby\Cms\App;

class DefaultFields
{
    /**
     * @param $site
     * @param $pages
     * @param $page
     * @return array
     */
    public static function meta($site, $pages, $page): array
    {
        $title = $page->title()->value();
        $metaTitle = $page->customTitle()->or($page->title() . ' – ' . $site->title())->value();
        $description = ($page->description() ?? $site->description())->value();
        $keywords = $page->keywords()->or($site->keywords()->or(null))->value();
        $template = $page->intendedTemplate()->name();

        return [
            'title' => $title,
            'metaTitle' => $metaTitle,
            'description' => $description,
            'keywords' => $keywords,
            'pageTemplate' => ucfirst($template),
        ];
    }

    /**
     * @param $site
     * @return array
     */
    public static function footer($site): array
    {
        return [];
    }

    public static function languages(App $kirby)
    {
        return $kirby
            ->languages()
            ->map(fn($language) => [
                'code' => $language->code(),
                'name' => $language->name(),
                'isDefault' => $language->isDefault()
            ])
            ->values();
    }

    public static function siteData()
    {
        return (new SiteData())->all();
    }
}
