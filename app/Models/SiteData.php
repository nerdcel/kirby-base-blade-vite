<?php

namespace Nerdcel\Models;

class SiteData
{
    protected ?string $config = null;
    protected array $siteData = [];
    public static $instance = null;

    public function __construct()
    {
        if (self::$instance !== null) {
            return;
        }

        self::$instance = $this;

        $this->config = kirby()->root('config');
        $this->siteData = require $this->config . '/site-data.php';
    }

    public function all()
    {
        return $this->siteData;
    }
}
