<?php

namespace Nerdcel\Blade;

class Defs
{
    /**
     * @var array
     */
    private array $definitions;

    /**
     * @param array $defs
     */
    public function __construct(array $defs = [])
    {
        $this->definitions = $defs;
    }

    /**
     * @return array|\Closure[]
     */
    public function all()
    {
        return $this->definitions;
    }
}
