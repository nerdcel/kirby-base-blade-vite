<?php

namespace Nerdcel\Blade;

use Afbora\View\Compiler\BladeCompiler;
use Illuminate\Filesystem\Filesystem;
use Kirby\Toolkit\Obj;
use Nerdcel\Exceptions\BladeException;

class Blade
{
    private BladeCompiler $compiler;

    protected static $instance;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {
        $this->compiler = new BladeCompiler(new Filesystem(), kirby()->root('cache') . '/blade/snippets');

        foreach (Directives::getInstance()->all() as $directive => $callback) {
            $this->compiler->directive($directive, $callback);
        }
    }

    /**
     * @param string $path
     * @param string $view
     * @return Obj
     * @throws BladeException
     */
    public function useView(string $path, string $view): Obj
    {
        $filename = $this->getView($path, $view);

        $this->render($filename);

        return new Obj([
            'path' => $this->getCompiledPath($filename)
        ]);
    }

    /**
     * @param string $path
     * @param string $view
     * @return string
     * @throws BladeException
     */
    private function getView(string $path, string $view): string
    {
        $filename = "${path}${view}.blade.php";

        if (file_exists($filename)) {
            return $filename;
        }

        throw new BladeException('View not found');
    }

    /**
     * @param string $path
     * @return void
     */
    private function render(string $path)
    {
        $this->compiler->compile($path);
    }

    private function getCompiledPath(string $path)
    {
        return $this->compiler->getCompiledPath($path);
    }
}
