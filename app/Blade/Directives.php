<?php

namespace Nerdcel\Blade;

class Directives extends Defs
{
    protected static $instance;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {
        parent::__construct([
            'siteTitle' => $this->siteTitle(),
            'meta' => $this->meta(),
            'dd' => $this->dd(),
            'level' => $this->level(),
            'vueData' => $this->vueData(),
            'vueMethod' => $this->vueMethod(),
            'snippet' => $this->snippet(),
            'introtext' => $this->introtext(),
        ]);
    }

    private function meta()
    {
        return static function ($method) {
            if (method_exists(page()->meta(), $method)) {
                return '<?= $page->meta()->' . $method . '(); ?>';
            }
            return '';
        };
    }

    private function siteTitle(): \Closure
    {
        return static function () {
            return '<?= $site->title()->or("' . env('KIRBY_TITLE') . '")->escape(); ?>';
        };
    }

    private function dd(): \Closure
    {
        return static function () {
            $args = explode(',', func_get_args()[0]);
            $args[0] = trim($args[0]);
            $args[1] = isset($args[1]) ? trim($args[1]) : 0;
            return "<?php $args[1]? dump(json_decode($args[0])) : dump($args[0]); die(); ?>";
        };
    }

    private function level(): \Closure
    {
        return static function () {
            $args = explode(',', func_get_args()[0]);
            return "<<?=$args[0]?>><?=$args[1]?></<?=$args[0]?>>";
        };
    }

    private function vueData(): \Closure
    {
        return static function (string $data) {
            return "<script type=\"text/javascript\">
                window.AppData.setData($data);
            </script>";
        };
    }

    private function vueMethod(): \Closure
    {
        return static function (string $method) {
            return "<script type=\"text/javascript\">
                window.AppData.setMethod($method);
            </script>";
        };
    }

    private function snippet(): \Closure
    {
        return static function () {
            $args = trim(func_get_args()[0]);
            $env = '"__env" => $__env';

            if (str_ends_with($args, ']')) {
                $args = substr_replace($args, ', ' . $env, strlen($args) - 1, 0);
            } else {
                $args .= ', [' . $env . ']';
            }

            return "<?php echo snippet($args) ?>";
        };
    }

    private function introtext(): \Closure
    {
        return static function ($text) {
            return "<?php
            \$lines = explode('\n', $text);
            \$lines = array_map(function (\$line) {
                \$ktLine = kirbytextinline(\$line);
                return \"<span>\${ktLine}</span>\";
            }, \$lines);
            \$lines = implode('\n', \$lines);

            echo \$lines; ?>";
        };
    }
}
