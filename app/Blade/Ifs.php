<?php

namespace Nerdcel\Blade;

class Ifs extends Defs
{
    protected static $instance;

    /**
     * gets the instance via lazy initialization (created on first usage)
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {
        parent::__construct([

        ]);
    }
}
