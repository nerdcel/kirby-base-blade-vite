<?php

namespace Nerdcel\Composer;

class Hooks
{
    public static function linkAsset(): void
    {
        $cmd = '
        if [ -f .env ] && [ ! -d public/assets ]
        then
          . .env
          if [ $KIRBY_MODE == "development" ]
          then
            ln -s $PWD/src/assets ./public/assets
          fi
        fi';

        exec($cmd);
    }

    public static function linkLocalKirbyPlugins(): void
    {
        $cmd = '
        if [ -d app/Plugins ]
        then
          find $PWD/app/Plugins -maxdepth 1 -mindepth 1 -type d -exec ln -s \'{}\' $PWD/site/plugins/ \;
        fi';

        exec($cmd);
    }
}
