<?php

use Nerdcel\Blade\Blade;

Kirby::plugin('nerdcel/snippets', [
    'snippets' =>  (static function ($path, $blade) {
        $files = Kirby\Filesystem\Dir::read(__DIR__ . '/view');
        $blocks = [];

        array_map(static function ($snippet) use ($path, $blade, &$blocks) {
            $snippetName = str_replace('.blade.php', '', $snippet);
            $blocks[$snippetName] = $blade->useView($path, $snippetName)->path;
        }, $files);

        return $blocks;
    })(__DIR__ . '/view/', Blade::getInstance()),
]);
