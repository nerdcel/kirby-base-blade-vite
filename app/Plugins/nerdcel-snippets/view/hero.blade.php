<section class="intro">
    <div class="intro__content">
        <span class="rellax" data-rellax-speed="-2">{{ $block->headline() }}</span>
        <h1 class="rellax" data-rellax-speed="-2">
            @introtext($block->content()->text())
        </h1>
    </div>
    <img class="intro__bg" src="{{ $block->image()->toFiles()->first()->resize(1024)->url() }}"
         srcset="{{ $block->image()->toFiles()->first()->croppedImage()->srcset([
             '300w' => ['width' => 300 * 1.5],
             '600w' => ['width' => 600 * 1.5],
             '1024w' => ['width' => 1024 * 1.5],
             '1440w' => ['width' => 1440 * 1.5],
         ]) }}"
         alt=""
    >
</section>
