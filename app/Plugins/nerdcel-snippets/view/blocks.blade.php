@foreach($page->blocks()->toBlocks() as $block)
    @snippet($block->type(), ['block' => $block])
@endforeach
