<div class="layout">
  @foreach ($page->layoutblocks()->toLayouts() as $layout)
  <div class="layout__row" id="{{ $layout->id() }}">
    @foreach ($layout->columns() as $column)
    <section class="layout__col layout__col-{{ $column->span() }}/12">
      <div class="blocks">
        {!! $column->blocks() !!}
      </div>
    </section>
    @endforeach
  </div>
  @endforeach
</div>
