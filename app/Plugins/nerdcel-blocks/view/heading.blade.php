<header class="header w-4/5 {{ $block->textalign() }}">
  @level($block->level(), $block->text()->kt())
</header>
