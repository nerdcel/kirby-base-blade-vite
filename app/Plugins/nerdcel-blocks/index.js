panel.plugin("nerdcel/blocks", {
  blocks: {
    editor: `
      <k-input
        :value="content.text"
        type="textarea"
        @input="update({ text: $event })"
      />
    `,
    link: `
      <k-text><a :href="content.link">{{ content.linktext }}</a></k-text>
    `,
    heading: `
      <k-writer
        ref="input"
        :inline="true"
        :marks="true"
        placeholder="Text eingeben"
        :value="content.text"
        @input="update({ text: $event })"
        :class="content.textalign"
      />
    `
  }
});
