<?php

namespace Nerdcel\Extensions;

use arnoson\KirbyVite\Vite as ViteBase;

class Vite extends ViteBase
{
    public function isDevMode(): bool
    {
        return $this->isDev();
    }

    public function server(): string
    {
        return $this->devServer();
    }
}
