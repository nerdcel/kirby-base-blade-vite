<?php

use Nerdcel\Models\DefaultFields;

return function ($site, $pages, $page): array {
    return [
        'siteData' => DefaultFields::siteData()
    ];
};
