<?php

use Nerdcel\Blade\Directives;
use Nerdcel\Blade\Ifs;

return [
    'debug' => env('KIRBY_MODE') === 'development' || env('KIRBY_DEBUG', false),

    'languages' => env('KIRBY_MULTILANG', false),
    'languages.detect' => env('KIRBY_MULTILANG_DETECT', false),

    'panel' => [
        'install' => env('KIRBY_PANEL_INSTALL', false),
        'slug' => env('KIRBY_PANEL_SLUG', 'panel')
    ],

    'api' => [
        'slug' => env('KIRBY_API_SLUG', 'api')
    ],

    'cache' => [
        'pages' => [
            'active' => env('KIRBY_CACHE', false),
            'ignore' => fn(\Kirby\Cms\Page $page) => kirby()->user() !== null || $page->title()->value() === 'kontakt'
        ]
    ],

    'routes' => [/** ADD Additional Routes Here! **/],

    'vite-routes' => [/** Vite Api Routes **/],

    'johannschopplich' => [
        'kirby-vite' => [
            'entry' => 'index.js'
        ]
    ],

    'arnoson.kirby-vite.legacy' => true,

    // See https://github.com/johannschopplich/kirby-extended/blob/main/docs/meta.md
    'kirby-extended' => [
        'meta' => [
            'defaults' => function (\Kirby\Cms\App $kirby, \Kirby\Cms\Site $site, \Kirby\Cms\Page $page) {
                $description = $page->description()->or($site->description())->value();

                $cover = $page->metacover()->or($site->metacover()->or(false));
                $cover = $cover->value() ? $cover->toFile()->crop(400)->url() : '/assets/logo.png';

                return [
                    'opengraph' => [
                        'image' => $cover,
                    ],
                    'twitter' => [
                        'image' => $cover,
                    ],
                    'jsonld' => [
                        'WebSite' => [
                            'url' => $site->url(),
                            'name' => $site->title()->value(),
                            'description' => $description
                        ]
                    ],
                ];
            }
        ]
    ],

    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => env('MAIL_HOST', 'localhost'),
            'port' => env('MAIL_PORT', 1025),
            'security' => env('MAIL_SECURITY', false),
            'auth' => env('MAIL_AUTH', false),
            'username' => env('MAIL_USER', ''),
            'password' => env('MAIL_PW', ''),
        ]
    ],

    'afbora.blade.directives' => Directives::getInstance()->all(),

    'afbora.blade.ifs' => Ifs::getInstance()->all(),

    'medienbaecker.autofavicon.text' => 'N',
    'medienbaecker.autofavicon.color' => '#000000',
    'medienbaecker.autofavicon.color_dark' => '#FFFFFF'
];
