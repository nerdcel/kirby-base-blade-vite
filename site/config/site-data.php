<?php

use Nerdcel\Models\DefaultFields;

return [
    // (1)
    // The following data is mandatory for the frontend router to initialize:
    'children' => site()
        ->children()
        ->published()
        ->map(fn ($child) => [
            'uri' => $child->uri(),
            'title' => $child->title()->value(),
            'isListed' => $child->isListed(),
            'template' => $child->intendedTemplate()->name(),
            'navigation' => $child->navigation()->or(false)->value(),
            'hasChildren' => $child->hasChildren(),
            'subnavtitle' => $child->subnavtitle()->or('')->value(),
            'children' => $child
                ->children()
                ->published()
                ->map(fn ($grandChild) => [
                    'uri' => $grandChild->uri(),
                    'title' => $grandChild->title()->value(),
                    'isListed' => $grandChild->isListed(),
                    'template' => $grandChild->intendedTemplate()->name(),
                    'category' => option('category-subpages')[$grandChild->category()->or('uncategorized')->value()] ?? ucfirst($grandChild->category()->or('uncategorized')->html()),
                ])->values()
        ])
        ->values(),
    // (2)
    // The following data is required for multi-language setups:
    'languages' => DefaultFields::languages(kirby()),
    // (3)
    // You can add custom commonly used data, as done for this starterkit:
    'title' => site()->title()->value(),
//    'social' => page('about')
//        ->social()
//        ->toStructure()
//        ->map(fn ($social) => [
//            'url' => $social->url()->value(),
//            'platform' => $social->platform()->value()
//        ])
//        ->values(),
    'footer' => DefaultFields::footer(site()),
];
