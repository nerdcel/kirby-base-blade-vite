@extends('layouts.default')

@section('hero')
  @snippet('blocks')
@endsection

@section('content')

  @snippet('layoutblocks')
@endsection
