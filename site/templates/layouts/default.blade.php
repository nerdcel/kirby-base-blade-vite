<!DOCTYPE html>
<html lang="{{ $kirby->languageCode() ?? 'en' }}">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@siteTitle()</title>

  {{--  See https://github.com/johannschopplich/kirby-extended/blob/main/docs/meta.md--}}
  @meta(robots)
  @meta(jsonld)
  @meta(social)

  <meta name="theme-color" content="#ffffff">
  <meta name="apple-mobile-web-app-capable" content="no">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta name="apple-mobile-web-app-title" content="@siteTitle()">

  <link rel="manifest" href="/manifest.json">

  @snippet('autofavicon')

  {!! vite()->client() !!}
  {!! vite()->css() !!}

</head>
<body>

  <div id="app" v-cloak>

    @include('partials.navigation')

    @yield('hero')

    @yield('content')
  </div>

  @include('partials.scripts')

</body>
</html>
