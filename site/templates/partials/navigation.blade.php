<nav class="main-nav" :class="{ 'nav-open': navigation.open }">

  <a href="/" @click.prevent="toggleNavigation" class="nav-burger"><span></span></a>

  <ul>
    @foreach($siteData['children'] as $child)
      @if($child['isListed'])
      <li>
        <a class="{{ $page->uri() === $child['uri'] ? 'underline' : '' }}"
         href="/{{ $child['uri'] }}">{{ $child['title'] }}</a></li>
      @endif
    @endforeach
  </ul>
</nav>

@once
    @push('scripts')
      @vueData({
        navigation: {
          open: false
        }
      })
      @vueMethod({
        toggleNavigation: function() {
          this.navigation.open = !this.navigation.open;
        }
      })
    @endpush
@endonce
