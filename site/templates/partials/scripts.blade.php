@once
  {!! vite()->js() !!}
  {!! vite()->legacyPolyfills() !!}
  <script type="text/javascript">
    window.AppData = {
      data: {},
      methods: {},
      setData: function (obj) {Object.assign(window.AppData.data, obj)},
      setMethod: function (method) {Object.assign(window.AppData.methods, method)},
    }
  </script>
  @stack('scripts')
  {!! vite()->js('liftoff.js') !!}
@endonce
