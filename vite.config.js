import liveReload from 'vite-plugin-live-reload'
import path, {resolve} from 'path'
import legacy from '@vitejs/plugin-legacy'
import vue from '@vitejs/plugin-vue'
import 'dotenv/config'
import Components from 'unplugin-vue-components/vite'
import ViteSvgIconsPlugin from 'vite-plugin-svg-icons'
import postcss from './postcss.config'
import {defineConfig} from 'vite'

process.env.VITE_BACKEND_URL = `http://${process.env.KIRBY_DEV_HOSTNAME}`;
process.env.VITE_BACKEND_API_SLUG = process.env.CONTENT_API_SLUG;
process.env.VITE_MULTILANG = process.env.KIRBY_MULTILANG;

const root = 'src';

export default defineConfig(({mode}) => ({
  root,

  base: mode === 'development' ? '/' : '/dist/',

  resolve: {
    alias: {
      '~/': `${resolve(__dirname, root)}/`,
      '@fonts': `${resolve(__dirname, root)}/assets/fonts/`,
      '@images': `${resolve(__dirname, root)}/assets/images/`,
      '@icons': `${resolve(__dirname, root)}/assets/icons/`,
      '@styles': `${resolve(__dirname, root)}/styles/`,
      '@component': `${resolve(__dirname, root)}/scripts/components/`,
      '@lib': `${resolve(__dirname, root)}/scripts/libs/`,
      '@tailwindConfig': resolve(__dirname, 'tailwind.config.js'),
      'vue': 'vue/dist/vue.esm-bundler.js',
    },
  },

  server: {
    // Only important if you use a non-localhost php server, like laravel valet:
    cors: true,
    hmr: {host: 'localhost'},
    port: 3000,
    strictPort: true,
    https: false,
  },

  build: {
    outDir: resolve(process.cwd(), 'public/dist'),
    emptyOutDir: true,
    manifest: true,
    rollupOptions: {
      input: {
        index: resolve(process.cwd(), 'src/index.js'),
        liftoff: resolve(process.cwd(), 'src/liftoff.js'),
      }
    },
  },

  plugins: [
    liveReload(['storage/content/**/*', 'site/**/*.php'], {root: process.cwd()}),

    legacy({
      targets: ['defaults', 'ie >= 11', 'ios_saf >= 10', 'last 2 versions', '> 2%'],
      additionalLegacyPolyfills: ['regenerator-runtime/runtime'],
    }),

    vue(),

    ViteSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [resolve(process.cwd(), 'src/assets/icons')],
      // Specify symbolId format
      symbolId: 'icon-[dir]-[name]',
    }),

    Components({
      dirs: [
        path.resolve('src/scripts/components'),
      ],
    }),
  ],

  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@use 'sass:math'; @import '~/styles/mixins'; @import '~/styles/base';`,
      },
    },
    postcss,
  },

  optimizeDeps: {
    include: ['vue', '@tailwindConfig'],
  }
}));
